//Focus directive to focus on the desired input field
googleApp.directive('focus', function() {
    return function(scope, element) {
        element[0].focus();
    }      
});

googleApp.directive('scrollup', function ($document) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                elm.bind("click", function () {

                	//Scroll the window to the desired position
                    function scrollToTop(element, to, duration) {
                        if (duration < 0) return;
                        var difference = to - element.scrollTop;
                        var perTick = difference / duration * 10;

                        setTimeout(function () {
                            element.scrollTop = element.scrollTop + perTick;
                            scrollToTop(element, to, duration - 10);
                        }, 10);
                    }

                    scrollToTop($document[0].body, 0, 100);
                });
            }
        };
});