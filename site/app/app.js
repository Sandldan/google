﻿var googleApp = angular.module('googleApp', ['ngRoute', 'ngAnimate', 'simplePagination']);

googleApp.config(['$routeProvider',
  function ($routeProvider, $locationProvider) {
      $routeProvider.
        when('/', {
            title: 'Google',
            templateUrl: 'html/splash.html',
            controller: 'splashCtrl'
        })
        .when('/search', {
            title: 'Search',
            templateUrl: 'html/search.html',
            controller: 'searchCtrl'
        })
  }])

