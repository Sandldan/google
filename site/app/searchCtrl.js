googleApp.controller('searchCtrl', function ($scope, Data, $rootScope, $routeParams, $location, Pagination) {

    $scope.search = $rootScope.firstinput;
    $scope.pagination = Pagination.getNew(7);

    $scope.search = function(){
        if($scope.fields.searchQuery.length > 1)
        {
            Data.post('search', {
                query: $scope.fields.searchQuery
            }).then(function (results) {
                
                if (results.status == "success")
                {
                    $scope.result = results.result;
                } 
                else 
                {
                    $scope.result = [];
                }

                $scope.pagination.numPages = Math.ceil($scope.result.length/$scope.pagination.perPage);
            });
        }
    };
});