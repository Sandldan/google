<?php

/*	This is the main index file from where the API functions will be called through the slim framework  */
require '../../vendor/autoload.php';

require_once 'dbConnect.php';
require_once 'functions.php';
require_once 'dbHandler.php';

//Initiating the slim API
$app = new \Slim\Slim(array(
    'mode' => 'development',
    'debug' => true
));

//Slim API calls and function
$app->post('/search', function() use ($app){
    $result = array();
    $db = new DbHandler();
    $search_string = json_decode($app->request->getBody())->query;
    
    if (strlen($search_string) >= 2 && $search_string !== ' ')
    {
        $result = $db->search('websites', $search_string);
    
        if($result == false)
        {
            $response["status"] = "error";
            $response["result"] = "Query term not found";
            echoResponse(201, $response);
        } 
        else 
        {
            $response["status"] = "success";
            $response["result"] = $result;
            echoResponse(200, $response);
        }
    }
    else
    {
        $response["status"] = "error";
        $response["message"] = "Too short search term";
        $response['result'] = '';
        echoResponse(201, $response);
    }
});

//Run Slim
$app->run();

?>