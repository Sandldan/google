<?php
	
	/*	Helper functions */
	function echoResponse($status_code, $response) {
	    $app = \Slim\Slim::getInstance();
	    // Http response code
	    $app->status($status_code);

	    //Setting response content type to json
	    $app->contentType('application/json');

	    echo json_encode($response);
	}


?>