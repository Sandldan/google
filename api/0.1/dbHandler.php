<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once 'dbConnect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }

    public function search($table, $search_string) {
        /*
        Query that matches the given string in the following order of relevance.
        #1 - Title
        #2 - Url
        #3 - Keywords
        */
        $query =   'SELECT *
                    FROM '. $table .'
                    WHERE title LIKE "%'. $search_string .'%" 
                    OR url LIKE "%'. $search_string .'%" 
                    OR keywords LIKE "%'. $search_string .'%"  
                    ORDER BY (CASE WHEN `title` = "'. $search_string .'" THEN 0
                                   WHEN `title` like "%'. $search_string .'%" THEN 1
                                   WHEN `title` like "%'. $search_string .'" THEN 2
                                   WHEN `url` like "%'. $search_string .'" THEN 3
                                   WHEN `url` like "%'. $search_string .'%" THEN 4
                                   WHEN `keywords` = "'. $search_string .'" THEN 5
                                   WHEN `keywords` like "%'. $search_string .'%" THEN 6
                                   ELSE 7
                              END)';

        $r = $this->conn->query($query) or die($this->conn->error.__LINE__);
    
        if(mysqli_num_rows($r) == 0)
        {
            return false;
        }
        else 
        {
            while($result = $r->fetch_assoc()){
                    $rows[] = $result;
                }
            return $rows; 
        }   
    }                       
}

?>
