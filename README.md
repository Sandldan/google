Google imitation
======
**Google imitiation** is a basic imitation of Google search, which searches through the database and presents the results in a Google style manner.

I decided to do the project while playing around with integrating AngularJS with the PHP micro framework Slim.

#### Screenshot
![Screenshot software](https://dl.dropboxusercontent.com/u/3110591/googlecopy.png "screenshot software")

## Download
* [Version 0.1](https://bitbucket.org/Sandldan/google/get/4d4169e97876.zip)

## Usage
```$ git clone https://Sandldan@bitbucket.org/Sandldan/google.git
...```

## Contributors

### Third party libraries
* see [AngularJS](https://angularjs.org/)
* see [Slim](http://www.slimframework.com/)
* see [Bootstrap](http://getbootstrap.com/)

## Version 
* Version 0.1

## Demo 
* [Demo](http://sandldan.com/google)

## How-to use this code
1. Edit api/config.php file to add your database connection details for the API.
2. Edit site/app/data.js to point to wherever you have the api/0.1/index.php file.

## API
Just a single API call which takes a posted string and returns the result from the database ordered by the following relevance.

1. Title
2. URL
3. Keywords

Example POST:
```json
{
	"query" : "adipiscing"
}
```
Response:
```json
{
    "status": "success",
    "result": [
        {
            "id": "83",
            "title": "Topdrive adipiscing molestie hendrerit",
            "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
            "url": "https://ebay.co.uk/cras.aspx",
            "rating": "5",
            "keywords": "vel augue vestibulum rutrum rutrum",
            "country": "Philippines",
            "temp_company": "Topdrive",
            "temp_words": "adipiscing molestie hendrerit"
        }
}
```

## Contact
#### Developer
* e-mail: anjohnso@abo.fi
* Skype: Sandldan